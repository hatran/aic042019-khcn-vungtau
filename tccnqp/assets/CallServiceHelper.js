﻿var AjaxCall = function (servicePathName, serviceParams, serviceAsync) {
    this._servicePathName = servicePathName;
    this._serviceParams = serviceParams;
    this._serviceAsync = (serviceAsync == undefined ? false : serviceAsync);
    return this;
};

AjaxCall.prototype = {
    callService: function (serviceCallSuccess) {
        var root = _Host;
        $.ajax({
            type: "POST",
            url: root + this._servicePathName,
            data: this._serviceParams,
            contentType: "application/json; charset=utf-8",
            //dataType: "json",
            //async: this._serviceAsync,
            success: serviceCallSuccess,
            error: function (e) {
                //console.log("Error in caling serice.");
            },
            beforeSend: function () {
                //console.log(1);
                $("body").append('<div class="wt-waiting wt-fixed wt-large"></div>');
            },
            complete: function (e) {
                $(".wt-waiting").remove();
            }
        });
        //$.ajax({
        //    url: root + this._servicePathName,
        //    type: "POST",
        //    data: this._serviceParams,
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: serviceCallSuccess
        //});
    }
}

// cal service không load wating
var AjaxCall1 = function (servicePathName, serviceParams, serviceAsync) {
    this._servicePathName = servicePathName;
    this._serviceParams = serviceParams;
    this._serviceAsync = (serviceAsync == undefined ? false : serviceAsync);
    return this;
};

AjaxCall1.prototype = {
    callService: function (serviceCallSuccess) {
        var root = _Host;
        $.ajax({
            type: "POST",
            url: root + this._servicePathName,
            data: this._serviceParams,
            contentType: "application/json; charset=utf-8",
            //dataType: "json",
            //async: this._serviceAsync,
            success: serviceCallSuccess,
            error: function (e) {
                //console.log("Error in caling serice.");
            },
        });
        //$.ajax({
        //    url: root + this._servicePathName,
        //    type: "POST",
        //    data: this._serviceParams,
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: serviceCallSuccess
        //});
    }
}

ConfirmDialogCheckSearch = function (title, contents, onOK, onCafuncel) {
    var res = true;
    $("body").append("<div id='windowpopup'></div>");
    var myWindow = $("#windowpopup");
    myWindow.kendoDialog({
        width: "400px",
        title: title,
        closable: true,
        modal: false,
        content: "<p class='center'>" + contents + "<p>",
        actions: [
            { text: 'Đồng ý', primary: true, action: onCafuncel },
            { text: 'Gởi tiếp', action: onOK }

        ],
        initOpen: function () {

        },
        open: function () {
            $("body").append("<div class='k-overlay'></div>")
        },
        close: function () {
            $("#windowpopup, .k-overlay").remove();
        },
        show: function () {

        },
        hide: function () {

        },
    });
}
ConfirmDialog = function (title, contents, onOK, onCafuncel) {
    var res = true;
    $("body").append("<div id='windowpopup'></div>");
    var myWindow = $("#windowpopup");
    myWindow.kendoDialog({
        width: "400px",
        title: title,
        closable: true,
        modal: false,
        content: "<p class='center' style='margin: 10px 0'>" + contents + "</p>",
        actions: [
            { text: 'Có', action: onOK },
            { text: 'Không', primary: true, action: onCafuncel }
        ],
        initOpen: function () {

        },
        open: function () {
            $("body").append("<div class='k-overlay'></div>")
        },
        close: function () {
            $("#windowpopup, .k-overlay").remove();
        },
        show: function () {

        },
        hide: function () {

        },
    });
}
AlertDialog = function (title, contents, onClose) {
    $("body").append("<div id='windowpopup2'></div>");
    var myWindow = $("#windowpopup2");
    myWindow.kendoDialog({
        width: "400px",
        title: title,
        closable: true,
        modal: false,
        content: "<p class='center' style='margin: 10px 0'>" + contents + "</p>",
        actions: [
            { text: 'Đóng', action: onClose },
        ],
        initOpen: function () {

        },
        open: function () {
            $("body").append("<div class='k-overlay'></div>")
        },
        close: function () {
            $("#windowpopup2, .k-overlay").remove();
        },
        show: function () {

        },
        hide: function () {

        },
    });
}
toRoman =function(num) {
    var result = '';
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var roman = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
    for (var i = 0; i <= decimal.length; i++) {
        while (num % decimal[i] < num) {
            result += roman[i];
            num -= decimal[i];
        }
    }
    return result;
}
function getDayLast(day, daynow) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date();
    if (daynow != undefined) {
        var arr = daynow.split(/[- :]/),
        firstDate = new Date(parseInt(arr[0]), parseInt(arr[1]) - 1, parseInt(arr[2]), parseInt(arr[3]), parseInt(arr[4]), parseInt(arr[5]));
    }
    var secondDate = new Date(day);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
    if (diffDays >= 1) {
        if (diffDays > 30) {
            return kendo.toString(new Date(day), "dd/MM/yyyy");
        }
        else {
            return diffDays + ' ngày trước';
        }
    }
    else {
        var oneHours = 60 * 60 * 1000; // minutes*seconds*milliseconds
        var diffHours = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneHours)));
        if (diffHours >= 1) {
            return diffHours + ' giờ trước';
        }
        else {
            var oneMinutes = 60 * 1000; // seconds*milliseconds
            var diffMinutes = (Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneMinutes)));
            if (diffMinutes >= 1) {
                return Math.round(diffMinutes) + ' phút trước';
            }
            else {
                return 'vừa xong';
            }
        }
    }
}
function getTheSubstring(value, length) {
    if (value == null) return "";
    if (value.length > length)
        return kendo.toString(value.substring(0, length)) + "...";
    else return kendo.toString(value);
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validatePhone(inputtxt) {
    var phoneno = /^[0-9\-\+]{9,15}$/;
    return phoneno.test(inputtxt);
}
function checkPassword(str) {
    // at least one number, one lowercase and one uppercase letter
    // at least six characters
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(str);
}